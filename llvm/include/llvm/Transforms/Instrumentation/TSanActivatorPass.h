#ifndef LLVM_TRANSFORMS_INSTRUMENTATION_TSANACTIVATORPASS_H
#define LLVM_TRANSFORMS_INSTRUMENTATION_TSANACTIVATORPASS_H

#include "llvm/IR/PassManager.h"

namespace llvm {

class TSanActivatorPass : public PassInfoMixin<TSanActivatorPass> {
    public:
        PreservedAnalyses run(Function &F, FunctionAnalysisManager &AM);
    
    private:
        const std::vector<std::string> ActivatorCalls = {
            "MPI_Win_create",
            "MPI_Win_allocate",
        };
};

} // namespace llvm

#endif // LLVM_TRANSFORMS_INSTRUMENTATION_TSANACTIVATORPASS_H