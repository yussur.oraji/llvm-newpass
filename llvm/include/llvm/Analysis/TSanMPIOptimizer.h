#ifndef LLVM_ANALYSIS_TSAN_MPIOptimizer_H
#define LLVM_ANALYSIS_TSAN_MPIOptimizer_H

#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/DependenceAnalysis.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/PassManager.h"
#include <map>
#include <vector>

namespace llvm {

class TSanMPIOptimizerAnalysis : public AnalysisInfoMixin<TSanMPIOptimizerAnalysis> {
    public:
        static llvm::AnalysisKey Key;

        enum ShResType {
            RemoteBuf, ReadBuf, WriteBuf, DirtyBuf
        };

        struct SharedResource {
            Value* V;
            ShResType Type;
            bool operator==(const SharedResource ShRes) {
                return this->V == ShRes.V;
            }
        } typedef SharedResource;

        //Result Type
        struct Result {
            std::vector<SharedResource> SharedResources;
            bool RemoteIsReadOnly;
            bool RemoteIsWriteOnly;
        } typedef TSanMPIOptResult;

        // Run Analysis
        Result run(Module &M, ModuleAnalysisManager &AM);

    private:
        // Result Store
        TSanMPIOptResult ResultVal;

        //Store var names containing shared resources
        std::vector<SharedResource> SharedResources;

        struct RecurseConfig {
            bool recurseUp; // Recurse over operands of current Value
            bool recurseDown; // Recurse over uses of current Value
            int depth; // Maximum depth for recursion
            bool allowGenerating; // Allow adding new values from this one
        };

        /* CONNECTIVENESS CONFIG START */

        // Recurse depth - Acceptable degree of seperation from confirmed shared resource
        const static int max_depth = 6;

        const RecurseConfig DefaultConfig = {
            false, // Most of the time unneeded, functionality captured by Alias Analysis
            true,
            max_depth,
            true
        };

        /* CONNECTIVENESS CONFIG END */

        struct RecurseValue {
            RecurseConfig config;
            SharedResource V;
            bool operator==(const RecurseValue RV) {
                return this->V.V == RV.V.V;
            }
        };

        // Used for Values with no name in IR
        int Sharedcounter = 0;

        const std::map<std::string,std::pair<int,ShResType>> SharedResourceFactories = {
            // Format: (Function name, (Parameter index with Shared Resource, Type created by Function))
            {"MPI_Win_create",      {0, ShResType::RemoteBuf}},
            {"MPI_Win_allocate",    {4, ShResType::RemoteBuf}},
            {"MPI_Win_get_attr",    {2, ShResType::RemoteBuf}},
            {"MPI_Put",             {0, ShResType::WriteBuf}},
            {"MPI_Rput",            {0, ShResType::WriteBuf}},
            {"MPI_Get",             {0, ShResType::ReadBuf}},
            {"MPI_Rget",            {0, ShResType::ReadBuf}},
            {"MPI_Accumulate",      {0, ShResType::WriteBuf}},
        };

        bool addUnique(std::vector<RecurseValue> &Set, RecurseValue V);

        bool addShRes(RecurseValue& V);

        // Recursively adds connected Values to whitelist according to Recursion Configuration
        int recurseGenerateWhitelist(SharedResource V, RecurseConfig C, AAResults& AA);

        // Helper Function that populates result struct
        void createResultStruct(bool RemoteIsReadOnly, bool RemoteIsWriteOnly);
};

} // namespace llvm

#endif // LLVM_ANALYSIS_TSAN_MPIOptimizer_H